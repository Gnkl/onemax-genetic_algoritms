### Todos
 
- [x] Make first version of genetic algorithm
- [x] Crossover function with probability of crossover point
- [x] Rename for readability
- [ ] Make plots with different values of parameters
